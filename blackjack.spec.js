
    
//     { val: 2, displayVal: "2", suit: "hearts" },
//   { val: 3, displayVal: "3", suit: "hearts" },
//   { val: 4, displayVal: "4", suit: "hearts" },
//   { val: 5, displayVal: "5", suit: "hearts" },
//   { val: 6, displayVal: "6", suit: "hearts" },
//   { val: 7, displayVal: "7", suit: "hearts" },
//   { val: 8, displayVal: "8", suit: "hearts" },
//   { val: 9, displayVal: "9", suit: "hearts" },
//   { val: 10, displayVal: "10", suit: "hearts" },
//   { val: 10, displayVal: "Jack", suit: "hearts" },
//   { val: 10, displayVal: "Queen", suit: "hearts" },
//   { val: 10, displayVal: "King", suit: "hearts" },
//   { val: 11, displayVal: "Ace", suit: "hearts" }

    describe('Black Jack Game Test', () => {
        describe('dealerShouldDraw function', () => {
            // 0, 9 passed in to function should return false
            it('0, 9 passed in to function should return false', () => {
                const dealerHand = [{ val: 9, displayVal: "9", suit: "hearts" },
                { val: 10, displayVal: "10", suit: "hearts" }]; 
                const result = dealerShouldDraw(dealerHand);
                expect(result).toBe(false);
            })
            it('Ace, 6 passed in to function should return true', () => {
                const dealerHand = [{ val: 11, displayVal: "Ace", suit: "hearts" }, { val: 6, displayVal: "6", suit: "hearts" }]; 
                const result = dealerShouldDraw(dealerHand);
                expect(result).toBe(true);
            })
            it('10, 6, Ace passed in to function should return false', () => {
                const dealerHand = [10, 6, 11]; // 27
                const result = dealerShouldDraw(dealerHand);
                expect(result).toBe(false);
            })
            it('2, 4, 2, 5 passed in should return true', () => {
                const dealerHand = [{ val: 2, displayVal: "2", suit: "hearts" },{ val: 4, displayVal: "4", suit: "hearts" },{ val: 2, displayVal: "2", suit: "hearts" },{ val: 5, displayVal: "5", suit: "hearts" }];
                const result = dealerShouldDraw(dealerHand);
                expect(result).toBe(true);
            })
        })
    })



    // describe('Black Jack Game Test', () => {
    //     describe('dealerShouldDraw function', () => {
    //         // 0, 9 passed in to function should return false
    //         it('0, 9 passed in to function should return false', () => {
    //             const dealerHand = [ace,6]; 
    //             const result = dealerShouldDraw(dealerHand);
    //             expect(result).toBe(false);
    //         })
    //     })
    // })