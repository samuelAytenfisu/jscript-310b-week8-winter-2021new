const myPromise = new Promise(function(resolve, reject) {
  setTimeout(function() {
    if(Math.random() <= 0.5) {
    reject();
  } else {
    resolve();
  }
  }, 1000);
});

myPromise
  .then( ()=> {
    console.log('fail');
  }).catch(() => {
    console.log('success')
  })

  .then(() => {
    console.log('COMPLETE');
  });

